﻿using System;
using UnityEngine;

public class Camera : MonoBehaviour
{
    private Vector3 _offset;
    private void Start()
    {
        _offset = transform.position - Player.Instance.transform.position;
    }

    private void Update()
    {
        transform.position = Player.Instance.transform.position + _offset;
    }
}