﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class Trigger : MonoBehaviourSingleton<Trigger>
{
    public event Action onTrigger;
    [SerializeField] private float radius;

    private void Update()
    {
        var colliders = Physics.OverlapSphere(transform.position, radius);
        foreach (var c in colliders)
        {
            if (c.gameObject.CompareTag("Player"))
            {
                onTrigger?.Invoke();
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}