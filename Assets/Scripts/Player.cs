using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviourSingleton<Player>
{
    [SerializeField] private CharacterController characterController;
    [SerializeField] private float speed;
    [SerializeField] private Animator animator;

    private void Update()
    {
        var x = Input.GetAxis("Horizontal");
        var y = Input.GetAxis("Vertical");

        var moveDir = new Vector3(x, 0, y);

        // rb.velocity = moveDir * speed + Vector3.up * rb.velocity.y;
        characterController.SimpleMove(moveDir * speed);
        if (moveDir != Vector3.zero)
        {
            animator.SetBool("Sprint", true);
            transform.forward = moveDir;
        }
        else
        {
            animator.SetBool("Sprint", false);
        }
    }
}
