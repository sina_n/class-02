﻿using System;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField]private Transform pivot;
    private void Start()
    {
        Trigger.Instance.onTrigger += OnTriggerActive;
    }

    private void OnTriggerActive()
    {
        Trigger.Instance.onTrigger -= OnTriggerActive;
        OpenDoor();
    }

    private async void OpenDoor()
    {
        var t = 0f;
        while (t < 2f)
        {
            var a = Time.deltaTime * 90f / 2f;
            transform.RotateAround(pivot.position, Vector3.up, a);
            await Task.Yield();
            t += Time.deltaTime;
        }
        
    }

    
    
}