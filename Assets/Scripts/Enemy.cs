using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private List<Transform> patrolPoints;
    [SerializeField] private float speed = 10;
    public int currentPointIndex = 0;
    public bool isMovingForward;

    private void Start()
    {
        CalculatePoints();
    }

    private float _t;

    private void Update()
    {
        _t += Time.deltaTime;
        Patrol();
    }

    private Transform _currentPoint;
    private Transform _nextPoint;
    private float _duration;

    public void CalculatePoints()
    {
        _t = 0;
        currentPointIndex = isMovingForward ? currentPointIndex + 1 : currentPointIndex - 1;
        var currentPoint = patrolPoints[currentPointIndex];
        var nextPointIndex = isMovingForward ? currentPointIndex + 1 : currentPointIndex - 1;
        if (isMovingForward)
        {
            if (nextPointIndex >= patrolPoints.Count)
            {
                isMovingForward = !isMovingForward;
                nextPointIndex = currentPointIndex - 1;
            }
        }
        else
        {
            if (nextPointIndex < 0)
            {
                isMovingForward = !isMovingForward;
                nextPointIndex = currentPointIndex + 1;
            }
        }

        var nextPoint = patrolPoints[nextPointIndex];

        var direction = nextPoint.position - currentPoint.position;
        var length = direction.magnitude;
        var duration = length / speed;

        _currentPoint = currentPoint;
        _nextPoint = nextPoint;
        _duration = duration;
    }

    public void Patrol()
    {
        var t = _t / _duration;
        if (t > 1)
        {
            CalculatePoints();
            return;
        }

        t = Mathf.Clamp(t, 0, 1f);

        transform.position = Vector3.Lerp(_currentPoint.position, _nextPoint.position, t);
        transform.forward = _nextPoint.position - _currentPoint.position;
    }
}